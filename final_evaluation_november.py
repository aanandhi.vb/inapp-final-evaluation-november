# InApp Final Evaluation - November
# Author      : Aanandhi V B
# Date        : November 25, 2020
# Description : A program to create different Pets
# Project URL : https://gitlab.com/aanandhi.vb/inapp-final-evaluation-november/-/blob/master/final_evaluation_november.py


class Pet:

    def __init__(self, species=None, name=""):

        self.name = name
        if species not in ('Dog', 'Cat', 'Horse', 'Hamster'): raise Exception("Invalid Species!")
        else: self.species = species

    def __str__(self):

        if self.name:
            return "Species: {}, named: {}".format(self.species, self.name)
        else:
            return "Species: {}, named: {}".format(self.species, "unnamed")


class Dog(Pet):

    def __init__ (self, species, name="", chases="Cats"):
        super().__init__(species, name)
        self.chases = chases

    def __str__(self):

        if self.name:
            return "Species: {}, named: {}, chases: {}".format(self.species, self.name, self.chases)
        else:
            return "Species: {}, named: {}, chases: {}".format(self.species, "unnamed", self.chases)


class Cat(Pet):

    def __init__(self, species, name="", hates="Dogs"):
        super().__init__(species, name)
        self.hates = hates

    def __str__(self):

        if self.name:
            return "Species: {}, named: {}, hates: {}".format(self.species, self.name, self.hates)
        else:
            return "Species: {}, named: {}, hates: {}".format(self.species, "unnamed", self.hates)


class Main:

    def execute(self):

        while True:

            print('''\n1. Create a Pet\n2. Create a Pet Dog\n3. Create a Pet Cat\n4. Exit''')
            user_choice = int(input("\nEnter your choice: "))

            if user_choice == 1:
                pet_name = input("\nEnter pet name: ")
                pet_species = input("\nEnter pet species: ")
                p = Pet(pet_species, pet_name) if pet_species and pet_name else Pet(pet_species) if pet_species else Pet(None, pet_name)
                print("Pet Status: ", p.__str__())

            elif user_choice == 2:
                pet_name = input("\nEnter dog name: ")
                pet_chases = input("\nEnter animal chased by dog: ")
                d = Dog('Dog', pet_name, pet_chases) if pet_chases and pet_name else Dog('Dog', pet_name) if pet_name else Dog('Dog', "", pet_chases) if pet_chases else Dog('Dog')
                print("Dog Status: ", d.__str__())

            elif user_choice == 3:
                pet_name = input("\nEnter cat name: ")
                pet_hates = input("\nEnter animal hated by cat: ")
                c = Cat('Cat', pet_name, pet_hates) if pet_hates and pet_name else Cat('Cat', pet_name) if pet_name else Cat('Cat', "", pet_hates) if pet_hates else Cat('Cat')
                print("Cat Status: ", c.__str__())

            else:
                break


m = Main()
m.execute()









